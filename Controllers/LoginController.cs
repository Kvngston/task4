﻿using System;
using System.Web.Mvc;
using DevOne.Security.Cryptography.BCrypt;
using LoginApp.DB;
using LoginApp.Models;
using MongoDB.Driver;

namespace LoginApp.Controllers
{
    public class LoginController : Controller
    {
        // GET

        [HttpGet]
        public ActionResult LoginPage()
        {
            return View("LoginPage");
        }

        [HttpPost]
        public ViewResult Login(string username, string password)
        {
            var database = Mongo.UserModel();
            var filter = Builders<User>.Filter.Eq("UserName", username);
            var user = database.FindSync<User>(filter).FirstOrDefault();

            if (user == null)
            {
                ViewBag.hasError = true;
                ViewBag.Error = "User not found";
                return View("LoginPage");
            }
            else
            {

                try
                {
                    if (BCryptHelper.CheckPassword(password, user.Password))
                    {
                        return View("DashBoard", user);
                    }
                    else
                    {
                        ViewBag.hasError = true;
                        ViewBag.Error = "Password Incorrect";
                        return View("LoginPage");
                    }
                }
                catch (Exception e)
                {
                    ViewBag.hasError = true;
                    ViewBag.Error = e;
                    return View("LoginPage");
                }
            }
        }
        

        public ViewResult RegisterPage()
        {
            User user = new User();
            return View(user);
        }

        [HttpPost]
        public ViewResult Register(User user, string confirmPassword)
        {
            if (user.UserName == null)
            {
                ViewBag.hasError = true;
                ViewBag.Error = "First Name cannot be Null";
                return View("RegisterPage");
            }
            var userModel = Mongo.UserModel();
            var filter = Builders<User>.Filter.Eq("UserName", user.UserName);

            try
            {
                var dataUser = userModel.FindSync<User>(filter).FirstOrDefault();
                if (dataUser == null)
                {
                    if (user.FirstName == null)
                    {
                        ViewBag.hasError = true;
                        ViewBag.Error = "First Name cannot be Null";
                        return View("RegisterPage");
                    }
                    if (user.LastName == null)
                    {
                        ViewBag.hasError = true;
                        ViewBag.Error = "Last Name cannot be Null";
                        return View("RegisterPage");
                    }
                    if (user.Password == null)
                    {
                        ViewBag.hasError = true;
                        ViewBag.Error = "Password cannot be Null";
                        return View("RegisterPage");
                    }

                    if (!user.Password.Equals(confirmPassword))
                    {
                        ViewBag.hasError = true;
                        ViewBag.Error = "Password MisMatch";
                        return View("RegisterPage");
                    }


                    var salt = BCryptHelper.GenerateSalt();
                    user.Password = BCryptHelper.HashPassword(user.Password, salt);
                    userModel.InsertOneAsync(user);
                }
                else
                {
                    ViewBag.hasError = true;
                    ViewBag.Error = "Username already exists";
                    return View("RegisterPage");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ViewBag.hasError = true;
                ViewBag.Error = e;
                return View("RegisterPage");
            }
            return View("LoginPage");
        }

        public ViewResult Logout()
        {
            return View("LoginPage");
        }
    }
}
