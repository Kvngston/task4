﻿using System;
using LoginApp.Models;
using MongoDB.Driver;

namespace LoginApp.DB
{
    public class Mongo
    {

        public static IMongoDatabase connect()
        {
            MongoClient dbClient = new MongoClient("mongodb://127.0.0.1:27017");

            if (dbClient.ListDatabases() == null)
            {
                Console.WriteLine("Something went wrong");
            }
            else
            {
                Console.WriteLine(dbClient.ListDatabases());
                Console.WriteLine("Connected successfully");
            }
            return dbClient.GetDatabase("AuthenticationApp");
        }

        public static IMongoCollection<User> UserModel()
        {
            var database = connect();
            return database.GetCollection<User>("Users");
        }
    }
}