﻿using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;

namespace LoginApp.Models
{
    public class User
    {

        public ObjectId Id;
        
        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }
        
        [Required]
        public string UserName { get; set; }
        
        [Required]
        public string Password { get; set; }

        public User()
        {    
            Id = new ObjectId();
        }

        public override string ToString()
        {
            return $"Username is {UserName}";
        }
    }
}